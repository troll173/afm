import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  FlatList,
  StyleSheet,
  TextInput,
  StatusBar,
  Alert,
} from 'react-native';
import {
  Avatar,
  Button,
  Card,
  Title,
  Paragraph,
  FAB,
  Searchbar,
  Dialog,
  Portal,
  ActivityIndicator,
  List,
} from 'react-native-paper';
import FilePickerManager from 'react-native-file-picker';
import storage from '@react-native-firebase/storage';
import RNFS, {moveFile} from 'react-native-fs';
import FileViewer from 'react-native-file-viewer';
import {launchImageLibrary} from 'react-native-image-picker';
import DocumentPicker from 'react-native-document-picker';
import TreeView from 'react-native-animated-tree-view';
import {
  uploadFileToFirebase,
  listFiles,
  listFolders,
  isUploadAllowed,
  addFolder,
  deleteFromStorage,
  getFileDownloadUrl,
  moveFileFromStorage,
} from '../../../services/FileService';
import * as Progress from 'react-native-progress';
import {Snackbar, Appbar} from 'react-native-paper';
import {theme} from '../../../themes/main';
import FileItem from '../../../components/FileItem';

import DashBar from '../../../components/DashBar';

import Toast from 'react-native-toast-message';

export default DocsTab = ({fileType}) => {
  const [searchQuery, setSearchQuery] = useState('');

  const onChangeSearch = query => setSearchQuery(query);

  const [files, setFiles] = useState([]);
  const [refreshing, setRefreshing] = useState(false);

  const [path, setPath] = useState(fileType);

  const [state, setState] = useState({open: false});

  const onStateChange = ({open}) => setState({open});

  const {open} = state;

  // Move Prompt

  const [isMovePromptVisible, setIsMovePromptVisible] = useState(false);

  const showMovePrompt = () => setIsMovePromptVisible(true);

  const hideMovePrompt = () => setIsMovePromptVisible(false);
  const [moveFolders, setMoveFolders] = useState([]);
  const [itemToMove, setItemToMove] = useState(null);

  // Create Folder Prompt
  const [folderName, setFolderName] = useState('');
  const [isFolderNamePromptVisible, setIsFolderNamePromptVisible] =
    useState(false);

  const showFolderNamePrompt = () => setIsFolderNamePromptVisible(true);

  const hideFolderNamePrompt = () => setIsFolderNamePromptVisible(false);

  // Rename Prompt
  const [askedName, setAskedName] = useState('');
  const [itemToRename, setItemToRename] = useState('');
  const [isAskNamePromptVisible, setIsAskNamePromptVisible] = useState(false);

  const showAskForNamePrompt = () => setIsAskNamePromptVisible(true);

  const hideAskForNamePrompt = () => setIsAskNamePromptVisible(false);

  const createFolder = async () => {
    console.log('creating folder');
    hideFolderNamePrompt();
    await addFolder(`${path}/${folderName}`);
    initFiles();
  };

  useEffect(() => {
    initFiles();
  }, [path]);

  function initFiles() {
    setRefreshing(true);
    listFiles(path).then(items => {
      setFiles(items);
      console.log(`Finished listing on ${path}: for ${fileType}`);
      setRefreshing(false);
    });
  }

  function changeDir(newPath) {
    setPath(newPath);
    initFiles();
  }

  function backwardsNavigate() {
    console.log(`navigate back from ${path}`);
    const pathArr = path.split('/').slice(0, -1);
    console.log(`pathArr ${pathArr}`);
    setPath(pathArr.join('/'));
    initFiles();
  }

  const selectFile = () => {
    FilePickerManager.showFilePicker(null, response => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled file picker');
      } else if (response.error) {
        console.log('FilePickerManager Error: ', response.error);
      } else {
        if (isUploadAllowed(fileType, response.type)) {
          const task = uploadFileToFirebase(path, response);
          Toast.show({
            type: 'info',
            text1: 'Uploading!',
            text2: 'File Uploading...',
          });
          task.on('state_changed', taskSnapshot => {
            console.log(
              `${taskSnapshot.bytesTransferred} transferred out of ${taskSnapshot.totalBytes}`,
            );
          });

          task.then(() => {
            Toast.show({
              type: 'success',
              text1: 'Upload Success!',
              text2: 'Document Uploaded!',
            });
            initFiles();
          });
        } else {
          Toast.show({
            type: 'error',
            text1: 'Upload Failed!',
            text2: 'File type not allowed!',
          });
        }
      }
    });
  };

  const _handleSearch = () => console.log('Searching');

  const openFileOrFolder = async item => {
    console.log(item);
    if (item.type === 'folder') {
      changeDir(`${path}/${item.name}`);
    } else {
      const localPath = `${RNFS.TemporaryDirectoryPath}/${item.name}`;
      await RNFS.downloadFile({
        fromUrl: await getFileDownloadUrl(`${path}/${item.name}`),
        toFile: localPath,
      }).promise;

      FileViewer.open(localPath, {
        displayName: item.name,
      });
    }
  };

  const deleteFile = async item => {
    console.log('delete', item);
    const confirm = await deleteConfirmationAlert(item);

    if (confirm) {
      try {
        await deleteFromStorage(`${path}/${item.name}`, item.type);
        setRefreshing(true);
        setTimeout(() => {
          initFiles();
        }, 5000);
      } catch (error) {
        errorAlert(error);
      }
    }
  };

  const errorAlert = errorMsg => {
    return new Promise((resolve, reject) => {
      Alert.alert(`Deletion Failed`, `${errorMsg}`, [
        {text: 'Ok', onPress: () => console.log('ok')},
      ]);
    });
  };

  const deleteConfirmationAlert = item => {
    return new Promise((resolve, reject) => {
      Alert.alert(
        `Delete ${item.type}`,
        `Are you sure you want to delete ${item.name}`,
        [
          {
            text: 'Cancel',
            onPress: () => resolve(false),
            style: 'cancel',
          },
          {text: 'Yes', onPress: () => resolve(true)},
        ],
      );
    });
  };

  const renameFile = item => {
    console.log('remame', item);
    setAskedName(item.name);
    setItemToRename(item);
    showAskForNamePrompt();
  };

  const onRenameConfirm = async () => {
    console.log(`rename from ${itemToRename.name} to ${askedName}`);
    hideAskForNamePrompt();
    setRefreshing(true);
    await moveFileFromStorage(
      `${path}/${itemToRename.name}`,
      `${path}/${askedName}`,
    );
    setRefreshing(false);
    initFiles();
  };

  const onMoveFile = async item => {
    await listFolders(path);
    const folders = files.filter(item => {
      return item.type == 'folder';
    });
    setItemToMove(item);
    setMoveFolders(folders);
    showMovePrompt();
    console.log('folders', folders);
  };

  async function moveItemToFolder(folder) {
    hideMovePrompt();
    setRefreshing(true);
    await moveFileFromStorage(
      `${path}/${itemToMove.name}`,
      `${path}/${folder}/${itemToMove.name}`,
    );
    setRefreshing(false);
    initFiles();
  }

  const renderItem = ({item}) => {
    return (
      <FileItem
        item={item}
        onDeleteFile={() => deleteFile(item)}
        onRenameFile={() => renameFile(item)}
        onMoveFile={() => onMoveFile(item)}
        onPress={() => openFileOrFolder(item)}
      />
    );
  };

  const moveItem = ({item}) => {
    return (
      <List.Item
        title={item.name}
        onPress={() => moveItemToFolder(item.name)}
        left={props => <List.Icon {...props} icon="folder" />}
      />
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <Appbar.Header theme={theme} statusBarHeight={0}>
        {path != fileType ? (
          <Appbar.BackAction
            onPress={() => {
              backwardsNavigate();
            }}
          />
        ) : null}
        <Appbar.Content
          titleStyle={{textTransform: 'capitalize'}}
          title={path}
        />
        <Appbar.Action icon="magnify" onPress={_handleSearch} />
      </Appbar.Header>

      <View style={styles.tabContainer}>
        <FlatList
          data={files}
          refreshing={refreshing}
          renderItem={renderItem}
          numColumns={4}
          onRefresh={() => initFiles()}
          keyExtractor={item => item.name}
        />
      </View>
      <Portal>
        <Dialog
          theme={theme}
          visible={isMovePromptVisible}
          onDismiss={hideMovePrompt}>
          <Dialog.Title>Select Move Location</Dialog.Title>
          <Dialog.Content>
            <FlatList
              data={moveFolders}
              renderItem={moveItem}
              keyExtractor={item => item.name}
            />
          </Dialog.Content>
        </Dialog>

        <Dialog
          theme={theme}
          visible={isFolderNamePromptVisible}
          onDismiss={hideFolderNamePrompt}>
          <Dialog.Title>Enter Folder Name</Dialog.Title>
          <Dialog.Content>
            <TextInput
              onChangeText={folderName => setFolderName(folderName)}
              placeholder="Folder Name"
            />
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={createFolder}>Confirm</Button>
          </Dialog.Actions>
        </Dialog>

        <Dialog
          theme={theme}
          visible={isAskNamePromptVisible}
          onDismiss={hideAskForNamePrompt}>
          <Dialog.Title>Enter Name</Dialog.Title>
          <Dialog.Content>
            <TextInput
              onChangeText={askedName => setAskedName(askedName)}
              placeholder="Rename File/Folder"
              defaultValue={askedName}
            />
          </Dialog.Content>
          <Dialog.Actions>
            <Button onPress={onRenameConfirm}>Confirm</Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>
      <FAB.Group
        fabStyle={styles.fab}
        open={open}
        icon={open ? 'close' : 'plus'}
        actions={[
          {
            icon: 'folder',
            label: 'Add Folder',
            onPress: () => showFolderNamePrompt(),
          },
          {
            icon: 'file',
            label: 'Add File',
            onPress: () => selectFile(),
          },
        ]}
        onStateChange={onStateChange}
        onPress={() => {
          if (open) {
            // do something if the speed dial is open
          }
        }}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  fab: {
    backgroundColor: theme.colors.primary,
  },
  dashContainer: {
    flex: 1,
    backgroundColor: theme.colors.primary,
    borderBottomWidth: 10,
    borderBottomColor: theme.colors.background,
    overflow: 'hidden',
  },
  tabContainer: {
    flex: 10,
    padding: 4,
    overflow: 'hidden',
  },
  searchContainer: {
    padding: 5,
  },
  searchBar: {
    // padding: 0
  },
});
