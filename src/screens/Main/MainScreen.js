import React, {useState} from 'react';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import TabScreen from './Tabs/TabScreen';
import TabHeader from '../../components/TabHeader';

import {theme} from '../../themes/main';

const MainScreen = () => {
  const Tab = createBottomTabNavigator();
  const [accountModalVisible, setAccountModalVisible] = useState(false);
  return (
    <Tab.Navigator
      screenOptions={{
        tabBarInactiveBackgroundColor: theme.colors.background,
        tabBarActiveBackgroundColor: theme.colors.background,
        tabBarActiveTintColor: theme.colors.primary,
      }}>
      <Tab.Screen
        name="Documents"
        options={{
          tabBarLabel: 'Docs',
          header: ({navigation, route, options}) => (
            <TabHeader
              navigation={navigation}
              options={options}
              route={route}
            />
          ),
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="file" color={color} size={size} />
          ),
        }}>
        {props => <TabScreen fileType="doc" {...props} />}
      </Tab.Screen>

      <Tab.Screen
        name="Audio"
        options={{
          tabBarLabel: 'Audio',
          header: ({navigation, route, options}) => (
            <TabHeader
              navigation={navigation}
              options={options}
              route={route}
            />
          ),
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons
              name="playlist-music"
              color={color}
              size={size}
            />
          ),
        }}>
        {props => <TabScreen fileType="audio" {...props} />}
      </Tab.Screen>

      <Tab.Screen
        name="Images"
        options={{
          tabBarLabel: 'Images',
          header: ({navigation, route, options}) => (
            <TabHeader
              navigation={navigation}
              options={options}
              route={route}
            />
          ),
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="camera" color={color} size={size} />
          ),
        }}>
        {props => <TabScreen fileType="image" {...props} />}
      </Tab.Screen>
    </Tab.Navigator>
  );
};

export default MainScreen;
