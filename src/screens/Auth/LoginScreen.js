import React, {useState} from 'react';
// import Button from "react-native-button";
import {Button} from 'react-native-paper';
import {Image, ImageBackground, Text, TextInput, View} from 'react-native';
import HideWithKeyboard from 'react-native-hide-with-keyboard';

import Logo from '../../../assets/icon.png';
import Bg from '../../../assets/splash.png';
import {styles} from './LoginStyle';

import * as authService from '../../services/AuthService';
import Toast from '../../components/Toast';

const LoginScreen = ({navigation: {push}}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const [visible, setVisible] = useState(false);
  const [msg, setMsg] = useState('');
  const [type, setType] = useState('error');

  return (
    <ImageBackground source={Bg} resizeMode="cover" style={styles.container}>
      <View style={styles.headerContainer}>
        <View style={{flex: 3}}>
          <Image source={Logo} style={styles.logo} />
        </View>

        <View style={{flex: 3}}>
          <Text style={styles.title}>Login</Text>
        </View>
      </View>

      <View style={styles.formContainer}>
        <Text style={styles.inputLabel}>Email</Text>
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.body}
            placeholder="E-mail"
            underlineColorAndroid="transparent"
            onChangeText={email => setEmail(email)}
            defaultValue={email}
          />
        </View>
        <Text style={styles.inputLabel}>Password</Text>
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.body}
            secureTextEntry={true}
            placeholder="Password"
            underlineColorAndroid="transparent"
            onChangeText={password => setPassword(password)}
            defaultValue={password}
          />
        </View>

        <View style={{flexDirection: 'row', width: '80%'}}>
          <View style={{flex: 1}}>
            <Text
              style={styles.forgot}
              onPress={() => {
                console.log('asasa');
              }}>
              Forgot Password?
            </Text>
          </View>
        </View>

        <Button
          mode="contained"
          style={styles.loginText}
          title="Log In"
          onPress={() => {
            if (email.length < 1 || password.length < 1) {
              setType('error');
              setVisible(true);
              setMsg('Invalid Input');
            } else {
              authService.emailAndPasswordLogin(email, password).then(res => {
                if (!res[0]) {
                  setType('error');
                  setVisible(true);
                  setMsg(res[1]);
                }
              });
            }
          }}>
          Log in
        </Button>
      </View>

      <HideWithKeyboard>
        <View style={styles.createContainer}>
          <Text
            style={styles.createText}
            onPress={() => {
              push('Register');
            }}>
            CREATE AN ACCOUNT
          </Text>
        </View>
      </HideWithKeyboard>

      <Toast visible={visible} msg={msg} type={type}></Toast>
    </ImageBackground>
  );
};

export default LoginScreen;