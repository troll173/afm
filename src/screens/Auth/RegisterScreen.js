import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  TextInput,
  ImageBackground,
  StyleSheet,
  ScrollView,
} from 'react-native';
import {Button, Checkbox, Dialog, Portal, Paragraph} from 'react-native-paper';
import HideWithKeyboard from 'react-native-hide-with-keyboard';
import {theme} from '../../themes/main';
import Logo from '../../../assets/icon.png';
import Bg from '../../../assets/splash.png';

import * as authService from '../../services/AuthService';

export default RegisterScreen = ({navigation: {pop}}) => {
  const [checked, setChecked] = useState(false);
  const [visible, setVisible] = useState(false);

  const showDialog = () => setVisible(true);

  const hideDialog = () => setVisible(false);

  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  return (
    <ImageBackground source={Bg} resizeMode="cover" style={styles.container}>
      <View style={styles.headerContainer}>
        <View style={{flex: 3}}>
          <Image source={Logo} style={styles.logo} />
        </View>

        <View style={{flex: 3}}>
          <Text style={styles.title}>Sign Up</Text>
        </View>
      </View>

      <View style={styles.formContainer}>
        <Text style={styles.inputLabel}>Name</Text>
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.body}
            placeholder="Name"
            underlineColorAndroid="transparent"
            onChangeText={name => setName(name)}
            defaultValue={name}
          />
        </View>

        <Text style={styles.inputLabel}>Email</Text>
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.body}
            placeholder="E-mail"
            underlineColorAndroid="transparent"
            onChangeText={email => setEmail(email)}
            defaultValue={email}
          />
        </View>

        <Text style={styles.inputLabel}>Password</Text>
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.body}
            secureTextEntry={true}
            placeholder="Password"
            underlineColorAndroid="transparent"
            onChangeText={password => setPassword(password)}
            defaultValue={password}
          />
        </View>

        <View style={styles.termsContianer}>
          <View style={{flex: 1}}>
            <Checkbox
              status={checked ? 'checked' : 'unchecked'}
              color={theme.colors.primary}
              onPress={() => {
                setChecked(!checked);
              }}
            />
          </View>

          <View
            style={{
              flex: 5,
              alignItems: 'flex-start',
              alignContent: 'center',
              paddingTop: 6,
            }}>
            <Text style={styles.termsText}>
              Agree to{' '}
              <Text
                style={{textDecorationLine: 'underline'}}
                onPress={showDialog}>
                Terms and Conditions
              </Text>
            </Text>
          </View>
        </View>

        <Button
          mode="contained"
          style={styles.signUpText}
          title="Log In"
          onPress={() => {
            if (!checked) {
              alert('Please agree to the terms and conditions');
            } else if (email.length < 1 || password.length < 1) {
              setType('error');
              setVisible(true);
              setMsg('Invalid Input');
            } else {
              let parent = this;
              authService
                .signUpWithEmailAndPassword(name, email, password)
                .then(res => {
                  if (!res[0]) {
                    parent.setType('error');
                    parent.setVisible(true);
                    parent.setMsg(res[1]);
                  }
                })
                .catch(err => {
                  console.error(err);
                });
            }
          }}>
          Sign Up
        </Button>
      </View>

      <HideWithKeyboard>
        <View style={styles.linkContainer}>
          <Text
            style={styles.linkText}
            onPress={() => {
              pop();
            }}>
            I HAVE AN ACCOUNT
          </Text>
        </View>
      </HideWithKeyboard>

      <Portal>
        <Dialog visible={visible} onDismiss={hideDialog}>
          <Dialog.Title>Terms and Conditions</Dialog.Title>
          <Dialog.Content>
            <ScrollView style={{height: '70%'}}>
              <Text style={{color: theme.colors.primary, fontSize: 17}}>
                Disclaimer
              </Text>
              <Paragraph>
                The information contained on the Service is for general
                information purposes only. The Company assumes no responsibility
                for errors or omissions in the contents of the Service. In no
                event shall the Company be liable for any special, direct,
                indirect, consequential, or incidental damages or any damages
                whatsoever, whether in an action of contract, negligence or
                other tort, arising out of or in connection with the use of the
                Service or the contents of the Service. The Company reserves the
                right to make additions, deletions, or modifications to the
                contents on the Service at any time without prior notice. The
                Company does not warrant that the Service is free of viruses or
                other harmful components.
              </Paragraph>
              <Text style={{color: theme.colors.primary, fontSize: 17}}>
                No Responsibility Disclaimer
              </Text>
              <Paragraph>
                The information on the Service is provided with the
                understanding that the Company is not herein engaged in
                rendering legal, accounting, tax, or other professional advice
                and services. As such, it should not be used as a substitute for
                consultation with professional accounting, tax, legal or other
                competent advisers. In no event shall the Company or its
                suppliers be liable for any special, incidental, indirect, or
                consequential damages whatsoever arising out of or in connection
                with your access or use or inability to access or use the
                Service.
              </Paragraph>
              <Text style={{color: theme.colors.primary, fontSize: 17}}>
                "Use at Your Own Risk" Disclaimer
              </Text>
              <Paragraph>
                All information in the Service is provided "as is", with no
                guarantee of completeness, accuracy, timeliness or of the
                results obtained from the use of this information, and without
                warranty of any kind, express or implied, including, but not
                limited to warranties of performance, merchantability and
                fitness for a particular purpose. The Company will not be liable
                to You or anyone else for any decision made or action taken in
                reliance on the information given by the Service or for any
                consequential, special or similar damages, even if advised of
                the possibility of such damages.
              </Paragraph>
            </ScrollView>
          </Dialog.Content>
          <Dialog.Actions>
            <Button color={theme.colors.primary} onPress={hideDialog}>
              Close
            </Button>
          </Dialog.Actions>
        </Dialog>
      </Portal>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: theme.colors.background,
  },
  headerContainer: {
    flexDirection: 'row',
    marginBottom: 30,
    marginTop: 10,
    paddingLeft: 20,
    paddingRight: 20,
    width: '100%',
    alignItems: 'center',
  },
  logo: {
    width: 64,
    height: 64,
  },
  title: {
    fontSize: 25,
    fontWeight: 'bold',
    color: theme.colors.primary,
    textAlign: 'right',
  },
  content: {
    paddingLeft: 50,
    paddingRight: 50,
    textAlign: 'center',
  },
  formContainer: {
    width: '100%',
    alignItems: 'center',
    flex: 1,
  },
  inputLabel: {
    marginTop: 10,
    marginBottom: 5,
    color: theme.colors.primary,
    width: '80%',
    textAlign: 'left',
  },
  inputContainer: {
    width: '80%',
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: 'grey',
    borderRadius: 10,
    backgroundColor: 'white',
    color: 'black',
  },
  body: {
    height: 42,
    paddingLeft: 20,
    paddingRight: 20,
    color: 'black',
  },
  linkContainer: {
    marginBottom: 60,
  },
  linkText: {
    color: theme.colors.primary,
    textDecorationLine: 'underline',
  },
  signUpText: {
    width: '80%',
    marginTop: 10,
    backgroundColor: theme.colors.primary,
    color: theme.colors.primaryText,
    fontWeight: 'bold',
    fontSize: 20,
  },
  termsContianer: {
    flexDirection: 'row',
    width: '70%',
    marginTop: 10,
  },
  termsText: {
    color: theme.colors.primary,
  },
});
