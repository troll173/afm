import {StyleSheet} from 'react-native';
import {theme} from '../../themes/main';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: theme.colors.background,
  },
  headerContainer: {
    flexDirection: 'row',
    marginBottom: 60,
    marginTop: 10,
    paddingLeft: 20,
    paddingRight: 20,
    width: '100%',
    alignItems: 'center',
  },
  logo: {
    width: 64,
    height: 64,
  },
  title: {
    fontSize: 25,
    fontWeight: 'bold',
    color: theme.colors.primary,
    textAlign: 'right',
  },
  content: {
    paddingLeft: 50,
    paddingRight: 50,
    textAlign: 'center',
  },
  loginText: {
    width: '80%',
    marginTop: 10,
    backgroundColor: theme.colors.primary,
    color: theme.colors.primaryText,
    fontWeight: 'bold',
    fontSize: 20,
  },
  formContainer: {
    width: '100%',
    alignItems: 'center',
    flex: 1,
  },
  inputLabel: {
    marginTop: 10,
    marginBottom: 5,
    color: theme.colors.primary,
    width: '80%',
    textAlign: 'left',
  },
  inputContainer: {
    width: '80%',
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: 'grey',
    borderRadius: 10,
    backgroundColor: 'white',
    color: 'black',
  },
  body: {
    height: 42,
    paddingLeft: 20,
    paddingRight: 20,
    color: 'black',
  },
  createContainer: {
    bottom: 60,
  },
  createText: {
    color: theme.colors.primary,
    textDecorationLine: 'underline',
  },
  forgot: {
    fontSize: theme.sizes.sm,
    color: theme.colors.primary,
    textAlign: 'right',
    textDecorationLine: 'underline',
  },
});
