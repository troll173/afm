import storage from '@react-native-firebase/storage';
import {getCurrentUser} from './AuthService';
import RNFS from 'react-native-fs';

function createReference(loc, root = false) {
  if (root) {
    console.log(`reference: /${loc}`);
    return storage().ref(`/${loc}`);
  } else {
    const user = getCurrentUser();
    console.log(`reference: ${user.uid}/${loc}`);
    return storage().ref(`${user.uid}/${loc}`);
  }
}

export function uploadFileToFirebase(filePath, file) {
  console.log('filePath', filePath);
  console.log('file', file);

  const reference = createReference(`${filePath}/${file.fileName}`);
  return reference.putFile(file.path);
}

export async function listFolders(path, pageToken) {
  const folders = await getFoldersInDir(path);
  console.log(folders);
  return folders;
}

async function getFoldersInDir(dir) {
  const reference = createReference(`${dir}`);

  const result = await reference.list({});

  const folders = await result._prefixes.map(async folder => {
    const folderName = folder.path.split(`\/${dir}\/`)[1];

    return {
      value: `${dir}/${folderName}`,
      name: folderName,
      items: await listFolders(`${dir}/${folderName}`),
    };
  });

  return Promise.all(folders);
}

export function listFiles(path, pageToken) {
  const reference = createReference(`${path}`);

  global.path = path;

  return reference.list({pageToken}).then(result => {
    if (result.nextPageToken) {
      return listFiles(reference, result.nextPageToken);
    }

    const files = result.items
      .filter(file => {
        return file.name != '.tmp';
      })
      .map(file => {
        return {
          path: path,
          name: file.name,
          type: 'file',
        };
      });

    const folders = result._prefixes.map(folder => {
      const folderName = folder.path.split(`\/${path}\/`)[1];
      return {
        path: path,
        name: folderName,
        type: 'folder',
      };
    });

    const items = [...folders, ...files];

    return Promise.resolve(items);
  });
}

export function isUploadAllowed(fileType, actualFileType) {
  switch (fileType) {
    case 'doc':
      return docTypes.includes(actualFileType);
      break;

    case 'audio':
      return audioTypes.includes(actualFileType);
      break;

    case 'image':
      return imageTypes.includes(actualFileType);
      break;
  }
}

export async function getFileDownloadUrl(loc, root = false) {
  const reference = createReference(`${loc}`, root);
  return await reference.getDownloadURL();
}

export async function moveFileFromStorage(loc, newLoc, root = false) {
  await copyFileFromStorage(loc, newLoc, root);
  await deleteFromStorage(loc, 'file', root);
}

export async function copyFileFromStorage(loc, newLoc, root = false) {
  const localPath = `${RNFS.TemporaryDirectoryPath}/fileToMove`;
  await downloadFileFromStorageToPhone(localPath, loc, root);
  const reference = createReference(`${newLoc}`, root);
  await reference.putFile(localPath);
}

export async function downloadFileFromStorageToPhone(
  localPath,
  loc,
  root = false,
) {
  await RNFS.downloadFile({
    fromUrl: await getFileDownloadUrl(loc, root),
    toFile: localPath,
  }).promise;
}

export async function deleteFromStorage(loc, type, root = false) {
  if (type === 'folder') {
    const hasFolder = await folderHasFolders(loc);
    // if (hasFolder) {
    //   throw `Can't delete folders within a folder!`;
    // } else {
    await deleteFolder(loc);
    // }
  } else {
    await deleteFile(loc);
  }
}

async function folderHasFolders(loc) {
  return new Promise((resolve, reject) => {
    listFiles(loc).then(items => {
      const folders = items.find(item => {
        return item.type === 'folder';
      });
      if (folders) {
        resolve(true);
      } else {
        resolve(false);
      }
    });
  });
}

async function deleteFile(loc, root = false) {
  console.log(`deleting FILE ${loc}`);
  const reference = createReference(`${loc}`, root);
  await reference.delete();
}

async function deleteFolder(loc) {
  console.log(`deleting FOLDER ${loc}`);

  await listFiles(loc).then(items => {
    items.forEach(async item => {
      await deleteFromStorage(`${item.path}/${item.name}`, item.type);
    });
  });

  try {
    await deleteTmp(loc);
  } catch (err) {
    console.log('Cant delete .tmp');
  }
}

async function deleteTmp(loc) {
  const reference = createReference(`${loc}/.tmp`);
  await reference.delete();
}

export async function addFolder(loc) {
  const localPath = `${RNFS.TemporaryDirectoryPath}/tmp.txt`;
  await downloadFileFromStorageToPhone(localPath, `tmp.txt`, true);
  const reference = createReference(`${loc}/.tmp`);
  await reference.putFile(localPath);
}

export function renameFolder(loc, newName) {}

const docTypes = [
  'text/csv',
  'application/pdf',
  'text/html',
  'application/msword',
  'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  'application/vnd.oasis.opendocument.spreadsheet',
  'application/x-httpd-php',
  'application/mspowerpoint',
  'application/vnd.ms-powerpoint',
  'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
  'application/vnd.openxmlformats-officedocument.presentationml.presentation',
  'application/x-mspublisher',
  'application/rtf',
  'application/x-rtf',
  'text/richtext',
  'text/plain',
  'application/excel',
  'application/vnd.ms-excel',
  'application/x-excel',
  'application/x-msexcel',
  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  'application/vnd.ms-excel.sheet.macroEnabled.12',
  'application/xml',
  'text/xml',
];
const audioTypes = [
  'audio/midi',
  'audio/mpeg3',
  'audio/mpeg',
  'audio/x-mpeg-3',
  'audio/ogg',
  'audio/wav',
  'audio/x-ms-wma',
];
const imageTypes = [
  'image/bmp',
  'application/postscript',
  'image/gif',
  'image/jpeg',
  'image/png',
  'application/octet-stream',
  'image/svg+xml',
  'image/tiff',
];
