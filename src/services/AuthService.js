import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

export const getCurrentUser = () => {
  return auth().currentUser;
};

export const emailAndPasswordLogin = (email, password) => {
  return new Promise((resolve, reject) => {
    console.log('credSS');
    console.log(email, password);

    auth()
      .signInWithEmailAndPassword(email, password)
      .then(response => {
        resolve([true, response]);
      })
      .catch(error => {
        switch (error.code) {
          case 'auth/wrong-password':
            resolve([false, 'Login Failed']);
            break;
          case 'auth/invalid-email':
            resolve([false, 'Please enter a valid email address']);
            break;
          default:
            reject(error);
            break;
        }
      });
  });
};

export const signUpWithEmailAndPassword = (name, email, password) => {
  return new Promise(async (resolve, reject) => {
    try {
      let res = await auth().createUserWithEmailAndPassword(email, password);
      console.log('User created', res);
      firestore()
        .collection('users')
        .add({
          name: name,
          email: email,
          uid: res.user.uid,
        })
        .then(response => {
          console.log('response', response);
          resolve([true, response]);
        })
        .catch(err => {
          console.log('ERROR', err);
          reject(error);
        });
    } catch (error) {
      if (error.code === 'auth/email-already-in-use') {
        resolve([false, 'That email address is already in use!']);
      } else if (error.code === 'auth/invalid-email') {
        resolve([false, 'That email address is invalid!']);
      } else {
        reject(error);
      }
    }
  });
};
