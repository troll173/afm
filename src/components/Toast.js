import React, {useState} from "react";
import { Snackbar } from 'react-native-paper';
import { StyleSheet } from 'react-native';

export default Toast = (prop) => {

    const [visible, setVisible] = useState(false);
    const [msg, setMsg] = useState('');

    const onDismissSnackBar = () => setVisible(false);
    
    const styles = StyleSheet.create({
        container: {
            backgroundColor: "#78C091",
        },
        dismiss: {
            color: 'black'
        }
    });

    const error = {
        colors: {
          onSurface: '#D10000',
          surface: 'white',
        },
      };

    const success = {
        colors: {
          onSurface: '#78C091',
          surface: 'white',
        },
      };  

      let toastType = success;
      switch(prop.type){
          case 'success':
            toastType = success;
          break;

          case 'error':
            toastType = error;
          break;
      }

    return(
        <Snackbar
            theme={toastType}
            visible={prop.visible}
            onDismiss={onDismissSnackBar}
            children={prop.msg}
            duration={300}
        >
        
      </Snackbar> 
    )
}