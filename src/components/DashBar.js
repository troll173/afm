import React, {useState} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {theme} from '../themes/main';
import {PieChart} from 'react-native-svg-charts';

export default DashBar = ({docs, audio, images, free}) => {
  const data = [docs, audio, images, free];
  const colors = ['#ffff', '#772F1A', '#DB5461', theme.colors.background];

  const pieData = data.map((value, index) => ({
    value,
    svg: {
      fill: colors[index],
    },
    key: `pie-${index}`,
  }));

  return (
    <View style={styles.container}>
      <PieChart style={styles.chart} data={pieData} />
      <View style={styles.legendContainer}>
        <View
          style={{
            borderStartWidth: 20,
            borderStartColor: colors[0],
            paddingLeft: 5,
            marginBottom: 5,
          }}>
          <Text style={{color: colors[0]}}>25% Documents</Text>
        </View>
        <View
          style={{
            borderStartWidth: 20,
            borderStartColor: colors[1],
            paddingLeft: 5,
            marginBottom: 5,
          }}>
          <Text style={{color: colors[1]}}>5% Audio</Text>
        </View>
        <View
          style={{
            borderStartWidth: 20,
            borderStartColor: colors[2],
            paddingLeft: 5,
            marginBottom: 5,
          }}>
          <Text style={{color: colors[2]}}>20% Images</Text>
        </View>
        <View
          style={{
            borderStartWidth: 20,
            borderStartColor: colors[3],
            paddingLeft: 5,
            marginBottom: 5,
          }}>
          <Text style={{color: colors[3]}}>50% Free</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: 15,
    flex: 1,
    flexDirection: 'row',
  },
  chart: {
    height: 100,
    flex: 2,
  },
  legendContainer: {
    flex: 4,
    paddingTop: 4,
  },
});
