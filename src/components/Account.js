import React from 'react';
import {ImageBackground, StyleSheet, Text, View} from 'react-native';
import Bg from '../../assets/splash.png';
import {theme} from '../themes/main';
import {styles} from '../screens/Auth/LoginStyle';

const Account = () => {
  return (
    <ImageBackground source={Bg} resizeMode="cover" style={styles.container}>
      <View style={styles.headerContainer}>
        <Text>Account</Text>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: theme.colors.background,
  },
  headerContainer: {
    flexDirection: 'row',
    marginBottom: 60,
    marginTop: 10,
    paddingLeft: 20,
    paddingRight: 20,
    width: '100%',
    alignItems: 'center',
  },
});

export default Account;
