import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Divider, IconButton, Menu} from 'react-native-paper';

import {theme} from '../themes/main';

import auth from '@react-native-firebase/auth';

import {getHeaderTitle} from '@react-navigation/elements';

const TabHeader = ({navigation, route, options}) => {
  const [visible, setVisible] = React.useState(false);

  const openMenu = () => setVisible(true);

  const closeMenu = () => setVisible(false);

  const title = getHeaderTitle(options, route.name);

  return (
    <View style={styles.container}>
      <View style={styles.leftItem}>
        <Text style={styles.title} children={title} />
      </View>

      <View style={styles.rightItem}>
        <Menu
          visible={visible}
          onDismiss={closeMenu}
          anchor={
            <IconButton
              size={25}
              color={theme.colors.primary}
              onPress={openMenu}
              style={styles.account}
              icon="account"
            />
          }>
          <Divider />
          <Menu.Item
            title="Logout"
            onPress={() => {
              auth()
                .signOut()
                .then(() => console.log('User signed out!'));
            }}
          />
        </Menu>
      </View>
    </View>
  );
};

export default TabHeader;

export const createTabHeader = (navigation, route, options) => {
  const title = getHeaderTitle(options, route.name);
  return <TabHeader navigation={navigation} route={route} option={options} />;
};

const styles = StyleSheet.create({
  container: {
    height: 50,
    width: '100%',
    backgroundColor: theme.colors.background,
    flexDirection: 'row',
  },
  account: {
    color: theme.colors.primary,
  },
  title: {
    fontSize: 20,
    color: theme.colors.primary,
  },
  leftItem: {
    flex: 4,
    paddingTop: 10,
    paddingLeft: 5,
  },
  rightItem: {
    flex: 2,
    alignItems: 'flex-end',
  },
});
