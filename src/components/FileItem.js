import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {Button, Badge, Menu, Divider, List} from 'react-native-paper';
import {Popover, PopoverController} from 'react-native-modal-popover';
import {theme} from '../themes/main';
import FileIcon from './FileIcon';

export default FileItem = ({
  item,
  onPress,
  onDeleteFile,
  onRenameFile,
  onMoveFile,
}) => {
  const fileName = truncateWithEllipses(item.name, 9);
  // console.log('item', item);

  return (
    <View style={styles.container}>
      <PopoverController>
        {({
          openPopover,
          closePopover,
          popoverVisible,
          setPopoverAnchor,
          popoverAnchorRect,
        }) => (
          <React.Fragment>
            <TouchableOpacity
              onPress={onPress}
              ref={setPopoverAnchor}
              onLongPress={openPopover}
              activeOpacity={0.6}>
              <FileIcon file={item} />
              <Text style={styles.name} children={fileName}></Text>
            </TouchableOpacity>

            <Popover
              contentStyle={styles.content}
              arrowStyle={styles.arrow}
              backgroundStyle={styles.background}
              visible={popoverVisible}
              onClose={closePopover}
              fromRect={popoverAnchorRect}
              supportedOrientations={['portrait', 'landscape']}>
              <List.Item
                title="Rename"
                onPress={item => {
                  onRenameFile(item);
                  closePopover();
                }}
                left={props => <List.Icon {...props} icon="border-color" />}
              />
              {item.type != 'folder' ? (
                <List.Item
                  title="Move"
                  onPress={item => {
                    onMoveFile(item);
                    closePopover();
                  }}
                  left={props => <List.Icon {...props} icon="flip-to-back" />}
                />
              ) : null}

              <List.Item
                title="Delete"
                onPress={item => {
                  onDeleteFile(item);
                  closePopover();
                }}
                left={props => <List.Icon {...props} icon="delete" />}
              />
            </Popover>
          </React.Fragment>
        )}
      </PopoverController>
    </View>
  );
};

function truncateWithEllipses(text, max) {
  return text.substr(0, max - 1) + (text.length > max ? '...' : '');
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 5,
    width: '25%',
    height: 100,
  },
  name: {
    textAlign: 'center',
    color: theme.colors.background,
  },
  badge: {
    position: 'absolute',
    bottom: 15,
    right: 10,
    backgroundColor: 'green',
  },
  content: {
    padding: 16,
    backgroundColor: theme.colors.card,
    borderRadius: 8,
  },
  arrow: {
    borderTopColor: theme.colors.card,
  },
  background: {
    backgroundColor: '#5c5c5c82',
  },
});
