import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';

const fileIcons = {
  ai: require('../../assets/icons/ai.png'),
  bmp: require('../../assets/icons/bmp.png'),
  csv: require('../../assets/icons/csv.png'),
  doc: require('../../assets/icons/doc.png'),
  docx: require('../../assets/icons/docx.png'),
  eps: require('../../assets/icons/eps.png'),
  gif: require('../../assets/icons/gif.png'),
  html: require('../../assets/icons/html.png'),
  jpg: require('../../assets/icons/jpg.png'),
  jpeg: require('../../assets/icons/jpeg.png'),
  midi: require('../../assets/icons/midi.png'),
  mp3: require('../../assets/icons/mp3.png'),
  ods: require('../../assets/icons/ods.png'),
  ogg: require('../../assets/icons/ogg.png'),
  pdf: require('../../assets/icons/pdf.png'),
  php: require('../../assets/icons/php.png'),
  png: require('../../assets/icons/png.png'),
  pps: require('../../assets/icons/pps.png'),
  ppsx: require('../../assets/icons/ppsx.png'),
  ppt: require('../../assets/icons/ppt.png'),
  pptx: require('../../assets/icons/pptx.png'),
  psd: require('../../assets/icons/psd.png'),
  pub: require('../../assets/icons/pub.png'),
  pubh: require('../../assets/icons/pubh.png'),
  pubm: require('../../assets/icons/pubm.png'),
  rtf: require('../../assets/icons/rtf.png'),
  svg: require('../../assets/icons/svg.png'),
  tif: require('../../assets/icons/tif.png'),
  tiff: require('../../assets/icons/tiff.png'),
  txt: require('../../assets/icons/txt.png'),
  wav: require('../../assets/icons/wav.png'),
  wma: require('../../assets/icons/wma.png'),
  xls: require('../../assets/icons/xls.png'),
  xlsm: require('../../assets/icons/xlsm.png'),
  xlsx: require('../../assets/icons/xlsx.png'),
  xml: require('../../assets/icons/xml.png'),
  folder: require('../../assets/icons/folder.png'),
};

export default FileIcon = ({file}) => {
  const fileArr = file.name.split('.');
  let ext = 'folder';
  if (file.type === 'file') {
    ext = fileArr[fileArr.length - 1];
  }
  return <Image style={styles.icon} source={fileIcons[ext]} />;
};

const styles = StyleSheet.create({
  icon: {
    height: 80,
    width: 72,
  },
});
