import { Platform, StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");
const SCREEN_WIDTH = width < height ? width : height;

export const theme = {
    colors: {
        primary: '#FF9F1C',
        primaryText: '#fffffcff',
        background: '#2D3333',
        card: '#e8e9ebff',
        text: '#fffffcff',
        border: 'black',
        notification: '#fffffcff',
    },
    sizes: {
        sm: 16,
        xl: 45 
    }
}