/**
 * @format
 * @flow strict-local
*/

import type {Node} from 'react';
import React, {useEffect, useState} from 'react';
import {ActivityIndicator} from 'react-native';

import LoginScreen from './src/screens/Auth/LoginScreen';
import RegisterScreen from './src/screens/Auth/RegisterScreen';

import MainScreen from './src/screens/Main/MainScreen';

import auth from '@react-native-firebase/auth';

import {CardStyleInterpolators, createStackNavigator} from '@react-navigation/stack';
import {theme} from './src/themes/main';

const App: () => Node = () => {
 
  const Stack = createStackNavigator();

    // Set an initializing state whilst Firebase connects
    const [initializing, setInitializing] = useState(true);
    const [user, setUser] = useState();
  
    // Handle user state changes
    function onAuthStateChanged(user) {
      setUser(user);
      if (initializing) setInitializing(false);
    }
  
    useEffect(() => {
      const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
      return subscriber; // unsubscribe on unmount
    }, []);

    // LOADING VIEW
  if (initializing){
    return(
      <ActivityIndicator size="large" color={theme.colors.primary} />
    )
  }
 
  if (user) {
   return(
     <MainScreen/>
   )
  }else{
    const config = {
      animation: 'spring',
      config: {
        stiffness: 1000,
        damping: 500,
        mass: 3,
        overshootClamping: true,
        restDisplacementThreshold: 0.01,
        restSpeedThreshold: 0.01,
      },
    };
    
    return(
      <Stack.Navigator initialRouteName="Login" screenOptions={{headerShown: false}}>
        <Stack.Screen name="Login" component={LoginScreen}
        animationEnabled="true"
         animationTypeForReplace="push"
        options={{
            title: 'Login',
            cardStyleInterpolator: CardStyleInterpolators.forFadeFromBottomAndroid,
          }}/>
        <Stack.Screen name="Register" component={RegisterScreen}
        animationEnabled="true"
        animationTypeForReplace="pop"
        options={{
            title: 'Register',
            cardStyleInterpolator: CardStyleInterpolators.forFadeFromBottomAndroid,
          }}/>
      </Stack.Navigator>
    )
  }
  
};

export default App;
