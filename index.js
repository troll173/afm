/**
 * @format
 */
import React from 'react';
import {Provider as PaperProvider} from 'react-native-paper';
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import 'react-native-gesture-handler';
import LogRocket from '@logrocket/react-native';
import Toast from 'react-native-toast-message';
import {NavigationContainer} from '@react-navigation/native';

export default function Main() {
  // LogRocket.init('xbajho/afm');
  global.path = '';
  return (
    <NavigationContainer>
      <PaperProvider>
        <App />
        <Toast />
      </PaperProvider>
    </NavigationContainer>
  );
}

AppRegistry.registerComponent(appName, () => Main);
